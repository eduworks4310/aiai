package aiai;


/**童謡アイアイを表示する
 * @author WE
 *
 */
public class Main {


	/**アイアイを表示
	 * @return
	 */
	public static String sayAiai(){
		return "アイアイ (アイアイ)";
	}
	/**同じメッセージを複数回表示
	 * @param msg
	 * @param count
	 * @return
	 */
	public static String repeatMessage(String msg,int count){
		String repeatMsg="";
		for(int i=0; i<count;i++){
			repeatMsg+=msg;
		}
		return repeatMsg;
	}

	/**改行する
	 * @return
	 */
	public static String newLine(){
		return "\n";
	}

	/**おさるさんを表示
	 * @return
	 */
	public static String sayMonkey(int num){
		if(num == 0){
			return "おさるさんだよ";
		}else{
			return "おさるさんだね";
		}

	}

	/**みなみのしまのを表示
	 * @return
	 */
	public static String saySouth(){
		return "みなみのしまの";
	}

	/**きのはのおうちを表示
	 * @return
	 */
	public static String sayLeaf(){
		return "きのはのおうち";
	}

	/**しっぽのながいを表示
	 * @return
	 */
	public static String sayLongTail(){
		return "しっぽのながい";
	}

	/**おめめのまるいを表示
	 * @return
	 */
	public static String sayEyes(){
		return "おめめのまるい";
	}

	/**アイアイを歌う処理
	 *
	 */
	public static void execute(){
		String lyric="";
		for(int i=0;i<2;i++){
		lyric+= repeatMessage(sayAiai(), 2);
		lyric+= newLine();
		lyric+= sayMonkey(i);
		lyric+= newLine();
		lyric+= repeatMessage(sayAiai(), 2);
		lyric+= newLine();
		if(i==0){
			lyric+= saySouth();
		}else{
			lyric+= sayLeaf();
		}

		lyric+= newLine();
		lyric+= repeatMessage(sayAiai(), 4);
		lyric+= newLine();
		if(i==0){
			lyric+= sayLongTail();
		}else{
			lyric+= sayEyes();
		}

		lyric+= repeatMessage(sayAiai(), 2);
		lyric+= newLine();
		lyric+= sayMonkey(i);
		lyric+= newLine();
		}
		System.out.println(lyric);

	}

	/**メインメソッド
	 * @param args
	 */
	public static void main(String[] args) {
		execute();
	}
}
